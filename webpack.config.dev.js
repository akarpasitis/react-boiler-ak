const { resolve } = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  target: 'web',
  context: resolve(__dirname, 'src'),
  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http:://localhost:8000',
    'webpack/hot/only-dev-server',
    './index.js'
  ],
  output: { filename: '[name]_[hash].js', path: resolve(__dirname, 'build', 'www'), publicPath: '/' },
  module: {
    rules: [
      { test: /\.json$/, loader: 'json-loader' },
      { test: /\.(gif|png|jpe?g|svg|woff(2)?|eot|ttf)$/i, loader: 'file-loader?name=assets/[name]_[hash].[ext]' },
      {
        test: /\.s?css$/,
        use: [
          'style-loader', 'css-loader?modules&localIdentName=[name]_[local]_[hash:base64:5]',
          'resolve-url-loader',
          'sass-loader'
        ]
      },
      { test: /\.js$/, use: 'babel-loader', exclude: /node_modules/ }
    ]
  },
  devtool: 'eval-source-map',
  devServer: {
    hot: true,
    contentBase: resolve(__dirname, 'src', 'www'),
    historyApiFallback: true,
    publicPath: '/',
    stats: {
      colors: true,
      hash: false,
      version: false,
      timings: true,
      assets: false,
      chunks: false,
      modules: false,
      reasons: false,
      children: false,
      source: false,
      errors: true,
      errorDetails: true,
      warning: true,
      publicPath: false
    }
  },
  resolve: { alias: { '~': resolve(__dirname, 'src') } },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: `"${process.env.NODE_ENV || 'development'}"` }
    }), // add css / bundle links to html
    new HtmlWebpackPlugin({ template: resolve(__dirname, 'src', 'www', 'index.html') }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin() // more readable modules in console
  ]
}